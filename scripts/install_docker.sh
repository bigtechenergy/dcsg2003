#!/bin/bash

sudo apt-get update && \
sudo apt-get install -y \
    apt-transport-https \
    ca-certificates \
    curl \
    gnupg-agent \
    software-properties-common && \

curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add - && \

sudo add-apt-repository \
   "deb [arch=amd64] https://download.docker.com/linux/ubuntu \
   $(lsb_release -cs) \
   stable" && \

sudo apt-get update && \
sudo apt-get install -y docker-ce docker-ce-cli containerd.io

sudo usermod -aG docker ubuntu

docker swarm join --token SWMTKN-1-5ajw1uzyx2xbvhqlku79zorybrnnlrdgu9fhk8ww7vk7ec4g4a-0gu0z3aay9h5cdkarot3kr15b 192.168.128.175:2377