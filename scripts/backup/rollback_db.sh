#!/bin/bash
set -euxo pipefail

# Stop db
ssh ubuntu@$DB1 "systemctl stop bookface"

RECOVERY_FILE=$(ssh ubuntu@$BACKUP "ls -t backups | head -n 1")
scp -3 ubuntu@$BACKUP:/home/ubuntu/backups/RECOVERY_FILE ubuntu@$DB1:/home/ubuntu/backup.tar.gz
ssh ubuntu@$DB1 "tar xvfz --overwrite backup.tar.gz"

ssh ubuntu@$DB1 "systemctl start bookface"