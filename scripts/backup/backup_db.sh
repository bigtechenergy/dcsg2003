#!/bin/bash
set -euxo pipefail

FILENAME="bfbackup_$(date +%Y-%m-%d-%H%M).tar.gz"
ssh ubuntu@$DB1 "tar -zcvpf \"$FILENAME\" --exclude \"bfdata/logs\" bfdata"
ssh ubuntu@$BACKUP "mkdir -p /home/ubuntu/backups"
scp -3 ubuntu@$DB1:/home/ubuntu/$FILENAME ubuntu@$BACKUP:/home/ubuntu/backups/$FILENAME

