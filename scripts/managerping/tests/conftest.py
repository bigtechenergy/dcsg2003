from pathlib import Path

import pytest


@pytest.fixture(scope="session")
def config_file(tmpdir_factory) -> Path:
    CONF_CONTENT = """network: imt3003
auth_url: https://api.skyhigh.iik.ntnu.no:5000
application_credential_id: mycredid
application_credential_secret: mycredsecret
manager_prefix: manager
www_prefix: www
timeout: 5
emails:
  - pedeha@stud.ntnu.no
  - patricek@stud.ntnu.no
  - anderscw@stud.ntnu.no
smtp_url: smtp.gmail.com
smtp_username: myuser@gmail.com
smtp_password: mypassword
ignored:
  - www3
noping: false
dryrun: false
"""
    t = tmpdir_factory.mktemp("conf").join("config.yml")
    t.write_text(CONF_CONTENT, encoding="utf-8")
    return t
