from pathlib import Path

import pytest
from managerping.config import Config, get_config
from managerping.fs import get_config_path


def test_get_config_path(config_file: Path) -> None:
    # TODO: More tests required to get full coverage
    assert get_config_path(config_file) == config_file


def test_get_config_full(config_file: Path) -> None:
    """Test get_config() with all args specified."""
    expected_config = Config(
        network="imt3003",
        auth_url="https://api.skyhigh.iik.ntnu.no:5000",
        application_credential_id="mycredid",
        application_credential_secret="mycredsecret",
        manager_prefix="manager",
        www_prefix="www",
        timeout=5,
        emails=list(set(["pedeha@stud.ntnu.no", "anderscw@stud.ntnu.no", "patricek@stud.ntnu.no"])),
        smtp_url="smtp.gmail.com",
        smtp_username="myuser@gmail.com",
        smtp_password="mypassword",
        ignored=["www3"],
        noping=False,
        dryrun=False,
    )
    assert get_config(config_file) == expected_config
