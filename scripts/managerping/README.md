# ManagerPing (title TBD)
### *Have you tried turning it on and off again?*

ManagerPing is a program developed for the course DCSG2003, Robust and Scalable Services (NOR: Robuste og skalerbare tjenester). The program uses the OpenStack API to retrieve all servers associated with an OpenStack project and checks that connectivity can be established.

The program uses the following methods to test connectivity:
* ICMP Ping
* HTTP Request (to web servers only)

Any servers that are unreachable or are suffering from poor connectivity are re-started. Servers that have been shut down are automatically powered on. 

Whenever the program takes action, it has the ability to notify users by mail.

### TODO:

- [ ] Log to disk 
- [ ] Custom `envvar_prefix`? https://click.palletsprojects.com/en/7.x/options/#values-from-environment-variables

# Installation
```bash
git clone https://gitlab.stud.iie.ntnu.no/bigtechenergy/dcsg2003
cd dcsg2003/scripts/managerping
poetry install
# OR pip install -r requirements.txt
python run.py
```

# Usage
```
Usage: run.py [OPTIONS]

Options:
  --emails TEXT        Email address(es) to notify. [env: MP_EMAILS]
  --network TEXT       OpenStack network name [env: MP_OS_NETWORK_NAME]
                       [default: imt3003]

  --url TEXT           OpenStack API Auth URL [env: MP_OS_AUTH_URL]
  --id TEXT            OpenStack API App Credentials ID [env:
                       MP_OS_APPLICATION_CREDENTIAL_ID]

  --secret TEXT        OpenStack API App Credentials secret [env:
                       MP_OS_APPLICATION_CREDENTIAL_SECRET]

  --manager TEXT       Manager VM name prefix [env: MP_MANAGER_PREFIX]
                       [default: manager]

  --www TEXT           Web Server VM name prefix [env: MP_WWW_PREFIX]
                       [default: www]

  --smtp TEXT          SMTP server URL [env: MP_SMTP_URL]  [default:
                       smpt.gmail.com]

  --smtpuser TEXT      SMTP username [env: MP_SMTP_USERNAME]
  --smtppassword TEXT  SMTP password [env: SMTP_PASSWORD]
  --config PATH        Custom config path [env: MP_CONFIG]
  --dryrun             Test servers but don't take any actions.
  --help               Show this message and exit.
```

The `--www` parameter defines the naming prefix of servers that should have HTTP requests sent to them.

The `--manager` parameter defines the prefix of servers that should be excluded from the connectivity check.

`[env:...]` in parameter descriptions indicate the corresponding environment variable that can be set, from which Managerping can automatically retrieve the appropriate parameter argument.

The `--config` parameter defines the path to a config file (see config section). If this parameter is defined, all other arguments are overriden by values in config.

# Running



## With Command-line Arguments

See `run_example.sh` and `env_example.sh` for more information.
```bash
python3 run.py \
    --emails pedeha@stud.ntnu.no \
    --emails some.other.mail@example.com \
    --url https://api.skyhigh.iik.ntnu.no:5000 \
    --network imt3003 \
    --id my_id \
    --secret my_secret \
    --smtp smtp.gmail.com \
    --smtpuser myemail@gmail.com \
    --smtppassword mypassword123
```

## With Config File

ManagerPing can run from a configuration file. Check `example_config.yml` for the structure of the configuration file.

```bash
python3 run.py --config config.yml
```

ManagerPing uses the following strategy to look for files:
1. Interpret config path as absolute (`<path>`)
2. Current working directory (`./<path>`)
2. ManagerPing dotdir (`~/.managerping/<path>`)
3. Home directory (`~/<path>`)