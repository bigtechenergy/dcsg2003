python3 run.py \
    --email pedeha@stud.ntnu.no \
    --email some.other.mail@example.com \
    --url https://api.skyhigh.iik.ntnu.no:5000 \
    --network imt3003 \
    --id my_id \
    --secret my_secret \
    --smtp smtp.gmail.com \
    --smtpuser myemail@gmail.com \
    --smtppassword mypassword123