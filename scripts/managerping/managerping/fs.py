"""
This program is intended to run on a Linux machine, but I added
platform-specific support for some reason.
"""
import sys
from dataclasses import dataclass
from pathlib import Path
from typing import Iterator

from ._types import PathType


@dataclass
class _Directories:
    ROOT_DIR: str = "~"
    DIRNAME: str = ".managerping"
    LOGDIR: str = "logs"

    @property
    def root(self) -> Path:
        """NOTE: Maybe delete this?"""
        return Path(self.ROOT_DIR).expanduser()

    @property
    def programdir(self) -> Path:
        return self.root / self.DIRNAME

    @property
    def logdir(self) -> Path:
        return self.programdir / self.LOGDIR


class LinuxDirs(_Directories):
    ROOT_DIR = "~/.local" # We use ~/.local in Linux
    DIRNAME = "managerping"


class WindowDirs(_Directories):
    pass


class MacDirs(_Directories):
    pass


if sys.platform == "win32" or sys.platform == "cygwin":
    _d = WindowDirs()
elif sys.platform == "linux":
    _d = LinuxDirs()
elif sys.platform == "darwin":
    _d = MacDirs()
else:
    _d = _Directories()


def get_log_dir(mkdir: bool=True) -> Path:
    """Returns path to log directory. 
    Creates the directory if it does not exist."""
    logdir = _d.logdir
    if not logdir.exists() and mkdir:
        logdir.mkdir(parents=True, exist_ok=True) # Return None if it fails?
    return logdir


def get_program_dir(mkdir: bool=True) -> Path:
    p = _d.programdir
    if not p.exists() and mkdir:
        p.mkdir(parents=True, exist_ok=True)
    return p

def get_config_path(config: PathType) -> Path:
    for p in _try_find_config_path(config):
        if p.exists():
            return p
    else:
        # Couldn't find the config
        raise ValueError(f"Unable to find '{config}'")

def _try_find_config_path(config: PathType) -> Iterator[Path]:
    yield Path(config)
    yield Path() / config
    yield get_program_dir(mkdir=False) / config
    yield (Path.home()) / config
