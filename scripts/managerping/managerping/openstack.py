import os
from typing import Optional

from keystoneauth1 import loading, session
from novaclient.client import Client  # NOT THE ACTUAL CLASS LOL


def get_openstack_client(
    auth_url: str, 
    application_credential_id: str, 
    application_credential_secret: str
) -> Client:
    """Creates a new OpenStack API client. 

    Parameters
    ----------
    auth_url : `str`
        OpenStack Auth URL.
    application_credential_id : `str`
        OpenStack Application Credentials ID.
    application_credential_secret : `str`
        OpenStack Application Credentials ID.

    Returns
    -------
    Client
        OpenStack API version 2 client
    """
    loader = loading.get_plugin_loader("v3applicationcredential")
    auth = loader.load_from_options(
        auth_url=auth_url,
        application_credential_id=application_credential_id,
        application_credential_secret=application_credential_secret,
    )
    sess = session.Session(auth=auth)
    return Client("2", session=sess)
