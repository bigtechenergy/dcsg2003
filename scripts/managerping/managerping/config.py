from __future__ import annotations

import re
from dataclasses import _MISSING_TYPE, Field, dataclass, field, fields
from pathlib import Path
from typing import List, Optional, TypedDict, Union

import yaml

try:
    from yaml import CDumper as Dumper
    from yaml import CLoader as Loader
except ImportError:
    from yaml import Loader # type: ignore
    from yaml import Dumper # type: ignore

import click

from ._types import PathType
from .fs import get_program_dir

email_pattern = re.compile(r"(^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-]+\.[a-zA-Z0-9-.]+$)")

ConfigValTypes = Union[str, int, List[str]]


def get_config(path: PathType) -> Config:
    """Retrieves a Config object from a path to a config file."""
    c = _ConfigParser(path)
    return c.get_config()


class _ConfigParser:
    def __init__(self, config_path: PathType) -> None:
        self.config = load_config_yaml(config_path)

    def get_config(self) -> Config:
        c = self.config
        f = get_config_value_or_default
        return Config( 
            emails=self._parse_emails(f(c, "emails")),  # type: ignore
            network=self._parse_network(f(c, "network")), # type: ignore
            auth_url=self._parse_auth_url(f(c, "auth_url")), # type: ignore
            application_credential_id=self._parse_application_id(f(c, "application_credential_id")), # type: ignore
            application_credential_secret=self._parse_application_secret(f(c, "application_credential_secret")), # type: ignore
            manager_prefix=self._parse_manager_prefix(f(c, "manager_prefix")), # type: ignore
            www_prefix=self._parse_www_prefix(f(c, "www_prefix")), # type: ignore
            smtp_url=self._parse_smtp_url(f(c, "smtp_url")), # type: ignore
            smtp_username=self._parse_smtp_username(f(c, "smtp_username")), # type: ignore
            smtp_password=self._parse_smtp_password(f(c, "smtp_password")), # type: ignore
            timeout=self._parse_timeout(f(c, "timeout")), # type: ignore
            ignored=self._parse_ignored(f(c, "ignored")), # type: ignore
            noping=self._parse_noping(f(c, "noping")), # type: ignore
            dryrun=self._parse_dryrun(f(c, "dryrun")), # type: ignore
        )
    
    def _parse_emails(self, emails: List[str]) -> List[str]:
        valid_emails = []
        for email in emails:
            if not email_pattern.match(email):
                click.echo(f"{email} is not a valid email address and will be ignored.", err=True)
            else:
                valid_emails.append(email)
        # Remove duplicates
        e = list(set(emails))
        if len(e) != len(valid_emails):
            click.echo("Ignoring duplicate email addresses.")
        return e
    
    def _parse_network(self, network: str) -> str:
        return network

    def _parse_manager_prefix(self, manager_prefix: str) -> str:
        return manager_prefix

    def _parse_www_prefix(self, www_prefix: str) -> str:
        return www_prefix

    def _parse_auth_url(self, auth_url: str) -> str:
        # TODO: add auth URL regex here
        return auth_url

    def _parse_application_id(self, application_id: str) -> str:
        return application_id # nothing here atm

    def _parse_application_secret(self, application_secret: str) -> str:
        return application_secret
    
    def _parse_smtp_url(self, smtp_url: str) -> str:
        return smtp_url or ""

    def _parse_smtp_username(self, smtp_username: str) -> str:
        return smtp_username or ""

    def _parse_smtp_password(self, smtp_password: str) -> str:
        return smtp_password or ""

    def _parse_timeout(self, timeout: int) -> int:
        # TODO: actually add decorators (yeah...) for these checks
        if not isinstance(timeout, int) or timeout < 0:
            # TODO: warn that negative timeout is not possible
            return _get_field_default(Config, "timeout")  # type: ignore
        return timeout
    
    def _parse_ignored(self, ignored: List[str]) -> List[str]:
        return ignored

    def _parse_noping(self, noping: bool) -> bool:
        return noping

    def _parse_dryrun(self, dryrun: bool) -> bool:
        return dryrun


@dataclass
class Config:
    network: str
    auth_url: str
    application_credential_id: str
    application_credential_secret: str
    manager_prefix: str = "manager"
    www_prefix: str = "www"
    timeout: int = 5
    emails: List[str] = field(default_factory=list)
    smtp_url: Optional[str] = None
    smtp_username: Optional[str] = None
    smtp_password: Optional[str] = None
    ignored: List[str] = field(default_factory=list)
    noping: bool = False
    dryrun: bool = False

    def _check_email_attrs(self) -> None:
        if self.can_send_email:
            return

        s = ["Email functionality disabled. Reasons: "]
        if not self.emails:
            s.append("No valid email addresses provided.")
        if not self.smtp_url:
            s.append("No SMTP URL.")
        if not self.smtp_username:
            s.append("No SMTP Username.")
        if not self.smtp_password:
            s.append("No SMTP Password.")

        click.echo(", ".join(s), err=True)
    
    @property
    def can_send_email(self) -> bool:
        """Checks if all mail-related attributes have a non-zero length."""
        return all(x is not None and len(x) > 0 for x in [self.emails, self.smtp_url, self.smtp_username, self.smtp_password]) 


########################################################################

# What I want to do is automatically generate a TypedDict class based on the fields
# in the Config dataclass. This is obviously not the correct way to do it,
# but serves to illustrate how I envision it. 
# Guess I need to re-read Fluent Python and Python Cookbook.

# _ConfigType = TypedDict(
#     "Config",
#     **{f.name: f.type for f in fields(Config)}
# )


class _ConfigType(TypedDict):
    network: str
    auth_url: str
    application_credential_id: str
    application_credential_secret: str
    manager_prefix: str
    www_prefix: str
    timeout: int
    emails: List[str]
    smtp_url: Optional[str]
    smtp_username: Optional[str]
    smtp_password: Optional[str]
    ignored: List[str]
    noping: bool
    dryrun: bool

########################################################################


def load_config_yaml(path: PathType) -> _ConfigType:
    with open(path, "r") as f:
        config = yaml.load(f, Loader=Loader)
    return config
    

def create_config_base(path: PathType = Path("config.yml"), exist_ok: bool=False) -> None:
    p = Path(path)
    if p.exists() and not exist_ok:
        click.echo(f"{p} already exists! Delete it or call create_config_base() with exist_ok=True.")
    p.write_text(
        yaml.dump(
            {f.name: _get_default(f) for f in fields(Config)},
            default_flow_style=False,
            Dumper=Dumper,
            sort_keys=False,
        )
    )


def get_config_value_or_default(config: _ConfigType, field_name: str) -> ConfigValTypes:
    """Attempts to retrieve the value of a `_ConfigType` dict entry, 
    if it cannot be found it falls back on the defaults defined in `Config`."""
    return config.get(field_name, _get_field_default(Config, field_name)) # type: ignore


def _get_field_default(cls: object, field_name: str) -> Optional[ConfigValTypes]:
    for f in fields(cls):
        if f.name == field_name:
            return _get_default(f)
    return None

#
#
# This deadass looks like reflection hell from Go
#
#

def _get_default(field: Field) -> Optional[ConfigValTypes]:
    if not isinstance(field.default, _MISSING_TYPE):
        return field.default
    # I don't understand why mypy complains about field.default_factory
    elif not isinstance(field.default_factory, _MISSING_TYPE): # type: ignore
        return field.default_factory()  # type: ignore
    elif isinstance(field.type, str):
        if "list" in field.type.lower():
            return []
        elif "dict" in field.type.lower():
            return {}
        elif field.type == "str":
            return str()
        elif field.type == "int":
            return int()
        else:
            # WE COULD DO eval(f"{field.type}()") but that seems dangerous
            return None # not sure about this one
    else:
        return field.type()
