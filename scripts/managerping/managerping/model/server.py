import asyncio
from dataclasses import dataclass, field
from functools import partial
from typing import List, Optional, Set

import httpx
from novaclient.exceptions import Conflict
from novaclient.v2.servers import REBOOT_HARD, REBOOT_SOFT, Server
from pythonping import ping

from ..cli import print_err, print_std
from ..enums import Measure, Status
from .action import Action
from .result import HTTPResults, PingResults


@dataclass
class OpenStackServer:
    server: Server
    network: str
    www_prefix: str
    noping: bool
    pings: Optional[PingResults] = None
    http_requests: Optional[HTTPResults] = None
    measures: Set[Measure] = field(default_factory=set)
    timeout: int = 5
    loop: asyncio.AbstractEventLoop = None
    
    @property
    def addresses(self) -> List[str]:
        return [ip["addr"] for ip in self.server.addresses[self.network]]
    
    def _ping_ok(self) -> bool:
        if self.noping:
            return True
        else:
            return self.pings is not None and self.pings.status == Status.OK
    
    def _http_ok(self) -> bool:
        if self.is_webserver:
            return self.http_requests is not None and self.http_requests.status == Status.OK
        else:
            return True

    @property
    def ok(self) -> bool:
        """Checks if all IPs were pinged and HTTP requested successfully."""
        return self._ping_ok() and self._http_ok()
   
    @property
    def is_webserver(self) -> bool:
        return self.server.name.startswith(self.www_prefix)

    @property
    def tested(self) -> bool:
        """Whether or not the connection has been tested."""
        return any(x is not None for x in [self.pings, self.http_requests])

    def __repr__(self) -> str:
        out = []
        out.append(f"Name: {self.server.name}")
        for ip in self.server.addresses[self.network]:
            ip_type = "Private" if ip["OS-EXT-IPS:type"] == "fixed" else "Floating"
            out.append(f"{ip_type}: {ip['addr']}")
        out.append(f"Status: {'Untested' if not self.tested else 'OK' if self.ok else 'Failed'}")
        out.append(f"Actions taken: {', '.join(str(m) for m in self.measures)}")
        return "\n".join(out)  

    async def test(self) -> None:
        """Sends a series of pings to the server. 
        Also sends an HTTP request if server is a web server."""
        print_std(f"Testing {self.server.name}...")
        if not self.noping:
            await self.ping()
        if self.is_webserver:
            await self.http()
        print_std(f"Finished testing {self.server.name}.")

    async def ping(self) -> None:
        if self.noping:
            print_err("Unable to ping with flag --noping enabled!")
            return

        for addr in self.addresses:
            to_run = partial(ping, addr, timeout=self.timeout)
            res = await self.loop.run_in_executor(None, to_run)
            self.pings = PingResults(addr, res)

    async def http(self, port: int=80) -> None:
        for address in self.addresses:
            res = HTTPResults(address, port)
            try:
                async with httpx.AsyncClient() as client:
                    r = await client.get(f"http://{address}:{port}")
            except (httpx.ConnectError, httpx.ConnectTimeout) as e:
                res.exception = e
            else:
                res.response = r
            self.http_requests = res # FIXME: overwriting if there are multiple IPs

    async def fix(self) -> None:
        """Attempts to fix any problems with the server. 
        Returns measure taken."""
        if self.ok:
            return None
        return await self.reboot()

    async def start(self, started_ok: bool=True) -> None:
        try:
            self.server.start()
            self.measures.add(Measure.START)
        except Conflict as e:
            if not started_ok:
                raise

    async def reboot(self, soft: bool=True, stopped_ok: bool=True) -> None:
        # TODO: verify that reboot was successful
        try:
            self.server.reboot(reboot_type=REBOOT_SOFT if soft else REBOOT_HARD)
            self.measures.add(Measure.REBOOT_SOFT if soft else Measure.REBOOT_HARD)
        except Conflict as e: # server is stopped
            # TODO: check what kind of Conflict we are dealing with
            # by checking e.code, e.message and e.details
            #
            # For now, we assume Conflict means the server is stopped,
            # and thus cannot be rebooted, which means we have to start it.
            if stopped_ok:
                await self.start()
                self.server.start()
                self.measures.add(Measure.START)
            else:
                # TODO: What to do if stopped_ok==False?
                pass

