from typing import Any

import click


def print_std(msg: Any, *args, **kwargs) -> None:
    click.echo(msg, *args, **kwargs)


def print_err(msg: Any, *args, **kwargs) -> None:
    click.echo(msg, *args, err=True, **kwargs)
