import asyncio
from typing import List

from .config import Config
from .dryrun import patch_servers
from .model.server import OpenStackServer
from .openstack import get_openstack_client


class ServerTester:
    def __init__(self, config: Config) -> None:
        self.config = config
        self.client = get_openstack_client(config.auth_url, config.application_credential_id, config.application_credential_secret)
        self.servers = self.get_servers()
        
        # Patch server objects if doing a dryrun
        if self.config.dryrun:
            patch_servers(self.servers)

    def get_servers(self) -> List[OpenStackServer]:
        return [
            OpenStackServer(
                server=server, 
                network=self.config.network, 
                www_prefix=self.config.www_prefix, 
                noping=self.config.noping,
                timeout=self.config.timeout
            ) 
            for server in self.client.servers.list() 
            if not server.name.startswith(self.config.manager_prefix)
            and not server.name in self.config.ignored
        ]

    def run(self) -> List[OpenStackServer]:
        return asyncio.run(self._run())

    async def _run(self) -> List[OpenStackServer]:
        return await self.test_servers(self.servers)
    
    async def _test_servers(self, servers: List[OpenStackServer]) -> List[OpenStackServer]:
        """Tests servers, returns servers that fail test."""
        await asyncio.gather(
            *[server.test() for server in servers]
        )
        return [server for server in servers if not server.ok]

    async def test_servers(self, servers: List[OpenStackServer]) -> List[OpenStackServer]:
        # Ping & send HTTP request to servers
        for server in servers:
            server.loop = asyncio.get_running_loop()

        failed = await self._test_servers(servers)

        # re-test failed pings
        # TODO: add sleep(?)
        failed = await self._test_servers(failed)

        return failed

    def fix_failed(self, servers: List[OpenStackServer]) -> List[OpenStackServer]:
        return asyncio.run(self._fix_failed(servers))

    async def _fix_failed(self, servers: List[OpenStackServer]) -> List[OpenStackServer]:
        await asyncio.gather(
            *[server.fix() for server in servers]
        )
        return servers
