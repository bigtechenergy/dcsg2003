from typing import List

from novaclient.v2.servers import Server

from .model.server import OpenStackServer


def patch_servers(servers: List[OpenStackServer]) -> None:
    for server in servers:
        _patch_server(server.server)


def _patch_server(server: Server) -> None:
    """Disables power-on state methods for a `Server` object."""
    def f(*args, **kwargs) -> None:
        pass
    setattr(server, "start", f)
    setattr(server, "reboot", f)
    setattr(server, "stop", f)
