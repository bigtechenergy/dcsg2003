"""
Script to ping all servers in a single OpenStack project.
Currently has no error handling functionality. 
Will fail on missing environment variables or invalid OpenStack credentials.

NOTE: MUST BE RUN AS ROOT USER. 
To keep own environment variables as root user run sudo with the -E flag
e.g. sudo -E python3 ping.py

TODO: Add concurrency or parallelism
"""
import asyncio
import os
from typing import Iterable, List, Optional

import click

from .cli import print_err
from .config import get_config
from .mail import EmailServer, notify_failure
from .tester import ServerTester


@click.command()
@click.option(
    "--config",
    help="Custom config path [env: MP_CONFIG]",
    envvar="MP_CONFIG",
    type=click.Path()
)
@click.option(
    "--dryrun",
    help="Test servers but don't take any actions.",
    is_flag=True,
)
@click.option(
    "--noping",
    help="Does not send ICMP requests. Useful if program cannot run as root user.",
    is_flag=True,
)
def run(
    config: str,
    dryrun: bool,
    noping: bool
) -> None:

    cfg = get_config(config)
    # CLI flags override config values
    if noping:
        cfg.noping = noping
    if dryrun:
        cfg.dryrun = dryrun

    # Test server connectivity
    tester = ServerTester(cfg)
    failed = tester.run()
    if not failed:
        click.echo("All servers were pinged successfully!")
        exit(0)
    
    # Attempt to fix failed servers
    failed = tester.fix_failed(failed)
    
    # List failed servers
    print_err("Failed:")
    print_err(failed)

    # Send notification for any failed servers
    notify_failure(failed, cfg)


if __name__ == "__main__":
    run()
