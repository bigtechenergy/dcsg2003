#!/bin/bash

# This script does the following:
#   * Downloads and installs CockroachDB
#   * Starts the bookface CockroachDB server
#   * Installs services for autostarting CockroachDB and synchronizing datetime

# Download and install CockroachDB
./install_cockroach.sh

# Start the bookface database
./start_bookface.sh

# Install services (CockroachDB and datetime synchronization)
./install_services.sh

# Install gluster
./install_gluster.sh