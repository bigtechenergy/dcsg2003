# Create new service that autostarts bookface using start_bookface.sh
sudo mv /home/ubuntu/bookface.service /etc/systemd/system/bookface.service
sudo systemctl enable bookface

# Create service that syncs time every 5 minutes
sudo mv /home/ubuntu/synctime.service /etc/systemd/system/synctime.service
sudo systemctl enable synctime
