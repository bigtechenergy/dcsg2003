#!/bin/bash

echo "Bookface auto start script"

cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=$WWW1:26257,$WWW2:26257,$WWW3:26257 --advertise-addr=$IP:26257