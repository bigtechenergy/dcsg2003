for IP in $WWW1 $WWW2 $WWW3
do
    # Export machine's own IP as an environment variable
    ssh -o "StrictHostKeyChecking no" ubuntu@$IP "echo export IP=$IP >> /home/ubuntu/.bashrc"
    
    # Send the required files
    scp ./install.sh ubuntu@$IP:/home/ubuntu/install.sh 
    scp ./install_cockroach.sh ubuntu@$IP:/home/ubuntu/install_cockroach.sh 
    scp ./bookface.service ubuntu@$IP:/home/ubuntu/bookface.service
    scp ./sync_time.sh ubuntu@$IP:/home/ubuntu/sync_time.sh
    scp ./synctime.service ubuntu@$IP:/home/ubuntu/synctime.service
    scp ./install_services.sh ubuntu@$IP:/home/ubuntu/install_services.sh
    
    # Create start_bookface.sh on the server
    ssh ubuntu@$IP "echo cockroach start --insecure --store=/bfdata --listen-addr=0.0.0.0:26257 --http-addr=0.0.0.0:8080 --background --join=$WWW1:26257,$WWW2:26257,$WWW3:26257 --advertise-addr=\"$(hostname -I\|cut -d" " -f 1)\":26257 > start_bookface.sh"
    
    # Run install script on the server
    ssh ubuntu@$IP "sudo screen -d -m ./install.sh" 

done

# Init DB on WWW1
scp ./init_db.sql ubuntu@$WWW1:/home/ubuntu/init_db.sql
ssh ubuntu@$WWW1 "sudo cockroach init --insecure --host=$WWW1:26257"

# Create tables on WWW1
ssh ubuntu@$WWW1 "sudo cockroach sql --insecure --host=localhost:26257 < ./init_db.sql"