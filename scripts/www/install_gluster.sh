#!/bin/bash

set -euxo pipefail

bf_brick="/home/ubuntu/bf_brick"
config_brick="/home/ubuntu/config_brick"
bf_config="/home/ubuntu/bf_config"
bf_images="/home/ubuntu/bf_images"

for IP in $WWW1 $WWW2 $WWW3
do
    ssh ubuntu@$IP "sudo apt-get -y install glusterfs-server glusterfs-client && sudo systemctl enable glusterd && sudo systemctl start glusterd"
    ssh ubuntu@$IP "mkdir -p $bf_brick $config_brick $bf_images $bf_config"
done

for IP in $WWW1 $WWW2 $WWW3
do
    # WWW1 command
    if [[ "$IP==$WWW1" ]]
    then
        CMD="sudo gluster peer probe $WWW2 && sudo gluster peer probe $WWW3"
    # WWW2 command
    elif [[ "$IP==$WWW2" ]]
    then
       CMD="sudo gluster peer probe $WWW1 && sudo gluster peer probe $WWW3"
    # WWW3 command
    else
        CMD="sudo gluster peer probe $WWW1 && sudo gluster peer probe $WWW2"
    fi
    ssh ubuntu@$IP $CMD
done

ssh ubuntu@$WWW1 "sudo gluster volume create bf_images replica 3 $WWW1:$bf_brick $WWW2:$bf_brick $WWW3:$bf_brick force"
ssh ubuntu@$WWW1 "sudo gluster volume start bf_images"
ssh ubuntu@$WWW1 "sudo gluster volume create bf_config replica 3 $WWW1:$config_brick $WWW2:$config_brick $WWW3:$config_brick force"
ssh ubuntu@$WWW1 "sudo gluster volume start bf_config"

for IP in $WWW1 $WWW2 $WWW3
do
    ssh ubuntu@$IP "sudo mount -t glusterfs $WWW1:bf_config $bf_config"
    ssh ubuntu@$IP "sudo mount -t glusterfs $WWW1:bf_images $bf_images"
done
